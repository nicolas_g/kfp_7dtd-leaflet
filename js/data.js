 /* */
var house = L.icon({
  iconUrl: 'images/marker_kfp_7dtd/house.png',
  iconSize: [16, 16],
  iconAnchor: [8, 8],
  popupAnchor: [0, -10]
});

var camp = L.icon({
  iconUrl: 'images/marker_kfp_7dtd/camp.png',
  iconSize: [16, 16],
  iconAnchor: [8, 8],
  popupAnchor: [0, -10]
});
    

var entities = [{
  "type": "FeatureCollection",
                                                                                   
  "features": [{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198009609645>ketchu13[FR]</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1000,1000]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198017995769>LMSypher</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1320,-1009]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198088069739>[=LM=] Panor</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1320,-1067]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198088069739>[=LM=] Panor</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1320,-1038]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198088069739>[=LM=] Panor</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1349,-1038]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198039830495>Didoum</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1832,1462]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198029746300>yanoos</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [505,1996]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198052628505>Melucta</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [655,1588]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [509,1007]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [511,999]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [524,1036]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [514,1036]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [524,1024]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [505,1024]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [526,1036]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [541,1036]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [541,1019]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [526,1019]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [501,1000]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [519,1031]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [507,1043]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [548,1012]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [525,992]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [493,992]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [548,1043]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197982255052>Pepito</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [338,1347]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1064,-1721]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1564,-1965]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1383,-1965]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1593,-2000]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1593,-1952]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1519,-1952]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1519,-2000]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1568,-1977]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1565,-1957]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197999804594>rhoxys</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1565,-1992]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198001454489>Berrabwyn</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [85,1791]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198001454489>Berrabwyn</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [94,1793]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198001454489>Berrabwyn</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [87,1804]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198001454489>Berrabwyn</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [80,1826]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198033234066>MasterPitchoune</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [511,1016]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [739,591]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [739,611]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [499,1234]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [488,1234]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [5119,-43402]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [10161,-59704]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [506,1248]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [508,1220]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [478,1229]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126363619>i have to</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [496,1259]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990183704>raphyto</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [417,1208]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990183704>raphyto</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [429,1201]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990798339>BubuMick</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [727,722]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990798339>BubuMick</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [727,732]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990798339>BubuMick</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [737,709]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990798339>BubuMick</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [717,709]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990798339>BubuMick</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [738,733]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990798339>BubuMick</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [716,733]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197968197169>Cumu[FR]</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1466,2157]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197968197169>Cumu[FR]</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1422,2172]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198030784563>jaggerjacks</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [264,377]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198126530745>gt01545</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1159,565]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990267899><==OoZeiusoO==></a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1767,1015]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990267899><==OoZeiusoO==></a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1767,1014]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197990267899><==OoZeiusoO==></a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1772,1018]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198011988717>papy plop</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1034,504]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198011988717>papy plop</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1020,501]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198011988717>papy plop</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [356,1158]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198027688826>Grummeld</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1048,290]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197968777190>Dem</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1743,1417]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198119112248>Fiiix</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-2351,111]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198015520289>ryoaspi</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1180,509]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198086759389>goldmika83</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [401,2111]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198086759389>goldmika83</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [415,2113]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198086759389>goldmika83</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [388,2115]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198086759389>goldmika83</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [402,2125]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198086759389>goldmika83</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [402,2103]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197968553623>Maxxwell</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [474,1133]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198062117868>Jeb</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1688,1465]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198062117868>Jeb</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [377,928]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198079656187>Kolenas</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [860,-172]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198079656187>Kolenas</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [860,-191]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198079656187>Kolenas</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [2958,-8644]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198079656187>Kolenas</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [2905,-8652]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198079656187>Kolenas</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [5909,-10811]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198079656187>Kolenas</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [5909,-10795]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198079656187>Kolenas</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [975,-171]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [676,617]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [635,638]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [635,597]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [664,613]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [661,638]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [704,1472]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [703,1472]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [703,1473]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [704,1473]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [6968,12169]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [6968,12158]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [6982,12164]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [650,650]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [659,638]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [675,671]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [611,814]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [611,797]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [594,797]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [594,814]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [594,829]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [611,829]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [657,559]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [661,536]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [662,594]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [666,654]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [644,657]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [662,624]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [635,616]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [590,1071]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198071983610>Nounours</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [601,1072]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198120314224>[=LM=] PARANO.maximus_skill</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1378,-1009]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197980357141>Ãelcan</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1275,1430]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197972024746>paca</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [614,1213]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197972024746>paca</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [614,1245]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197972024746>paca</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [636,1214]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197970276046>[=LM=] Popopelop î€“</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1378,-1067]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197970276046>[=LM=] Popopelop î€“</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1378,-1040]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197970276046>[=LM=] Popopelop î€“</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1349,-1009]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197970276046>[=LM=] Popopelop î€“</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1349,-1067]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198025541118>deepuru</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [797,-330]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197975841716>diablo846</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1914,821]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198018750574>BlueAngel</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-786,2173]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198018750574>BlueAngel</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-826,2159]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198018750574>BlueAngel</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-827,2211]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197996149706>matmat</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [802,436]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197996149706>matmat</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [408,1021]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197996149706>matmat</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-454,1782]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197996149706>matmat</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-464,1792]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197996149706>matmat</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-464,1772]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197996149706>matmat</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-444,1772]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197996149706>matmat</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-444,1792]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198009114257>Takiwaltek</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-2369,-1804]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198073963743>WtF.| Chuuk    â˜º [T]</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1841,1438]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198073963743>WtF.| Chuuk    â˜º [T]</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [1832,1436]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198039883059>willo130</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [262,-195]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197997607730>Mala</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [8429,26570]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198134643096>Deflows</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [436,953]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198027986912>Hostilis</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [343,1097]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198027986912>Hostilis</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [365,1097]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561198027986912>Hostilis</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [385,1106]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197992502355>L0chn3ss</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [335,319]
          }
    },
{
          "type": "Feature",
          "properties": {
              "name": "Barn",
              "popupContent": "<a href=http://steamcommunity.com/profiles/76561197967806994>Pot@g.be</a>",
              "entity": 1,
              "icon": house
          },
          "geometry": {
              "type": "Point",
              "coordinates": [-1822,-348]
          }
    }
 ]
}];