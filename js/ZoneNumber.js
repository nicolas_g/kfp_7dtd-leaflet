for (var k = 0; k < 64; k++) {
	var x1 = -16384 + k * 512;
	
	for (var i = 0; i < 64; i++) {
		var f  = x1 / 512.0;
		var g  = y1 / 512.0;
			if (f >= 0 && f < 1 ){
				f = 0;
			}
			poxlogx = Math.floor(f)

			if (g >= 0 && g < 1) {
				g = 0;
			}
			if (g < 0) {
				poxlogy = Math.floor(g - 1);
			}
			else{
				poxlogy = Math.floor(g);
			}
			if (f < 0) {
				poxlogx = Math.floor(f - 1);
			}
		var y1 = y1 = -16384 + i * 512;
			// Use a little math to position markers.
			// Replace this with your own code.
			L.marker([
				y1+128,
				x1+256
			], {
				icon: L.divIcon({
					// Specify a class name we can refer to in CSS.
					className: 'count-icon',
					// Define what HTML goes in each marker.
					html: (f ) + " " + (g + 1),
					// Set a markers width and height.
					iconSize: [30, 30]
				})
			}).addTo(map);
	}
}