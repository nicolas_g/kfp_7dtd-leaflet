L.Control.ZoomMin = L.Control.Zoom.extend({
  options: {
    position: "topleft",
    zoomInText: "+",
    zoomInTitle: "Zoom in",
    zoomOutText: "-",
    zoomOutTitle: "Zoom out",
    zoomMinText: "min",
    zoomMinTitle: "Zoom min",
    zoomMaxText: "max",
    zoomMaxTitle: "Zoom max"
  },

  onAdd: function (map) {
    var zoomName = "leaflet-control-zoom"
      , container = L.DomUtil.create("div", zoomName + " leaflet-bar")
      , options = this.options

    this._map = map

    this._zoomMaxButton = this._createButton(options.zoomMaxText, options.zoomMaxTitle,
     zoomName + '-max', container, this._zoomMax, this)
	
	this._zoomInButton = this._createButton(options.zoomInText, options.zoomInTitle,
     zoomName + '-in', container, this._zoomIn, this)

    this._zoomOutButton = this._createButton(options.zoomOutText, options.zoomOutTitle,
     zoomName + '-out', container, this._zoomOut, this)

    this._zoomMinButton = this._createButton(options.zoomMinText, options.zoomMinTitle,
     zoomName + '-min', container, this._zoomMin, this)

    this._updateDisabled()
    map.on('zoomend zoomlevelschange', this._updateDisabled, this)

    return container
  },

  _zoomMin: function () {
    this._map.setZoom(this._map.getMinZoom())
  },
  
  _zoomMax: function () {
    this._map.setZoom(this._map.getMaxZoom())
  },
  
  _updateDisabled: function () {
    var map = this._map
      , className = "leaflet-disabled"

    L.DomUtil.removeClass(this._zoomInButton, className)
    L.DomUtil.removeClass(this._zoomOutButton, className)
    L.DomUtil.removeClass(this._zoomMinButton, className)
	L.DomUtil.removeClass(this._zoomMaxButton, className)

    
	
	if (map._zoom === map.getMinZoom()) {
      L.DomUtil.addClass(this._zoomOutButton, className)
	  L.DomUtil.addClass(this._zoomMinButton, className)
	}

    if (map._zoom === map.getMaxZoom()) {
      L.DomUtil.addClass(this._zoomInButton, className)
	   L.DomUtil.addClass(this._zoomMaxButton, className)
	}

   }
})